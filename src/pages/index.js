import React from "react";

import Container from "../components/container";
import Header from "../components/header";
import Main from "../components/main"
import Footer from "../components/footer";

export default () => (
  <Container>
    <Header />
    <Main />
    <Footer />
  </Container>
);
