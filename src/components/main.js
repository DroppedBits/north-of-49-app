import React, { useState, useEffect } from "react";
import mainStyles from "./main.module.css";

import Apparent from "./apparent";
import Slider from "./slider";

const Main = () => {
  const [airTemperature, setAirTemperature] = useState(25.0);
  const [dewpoint, setDewpoint] = useState(15.0);
  const [relativeHumidity, setRelativeHumidity] = useState(60.0);
  const [windspeed, setWindspeed] = useState(5.0);

  const [apparentTemperatureCanada, setApparentTemperatureCanada] = useState(
    25.0
  );
  const [apparentTemperatureAmerica, setApparentTemperatureAmerica] = useState(
    25.0
  );
  
  useEffect(() => {
    if (airTemperature > 10) {
      const humidex = Math.round(calculateHumidex());
      const heatIndex = Math.round(calculateHeatIndex());
      setApparentTemperatureCanada(humidex);
      setApparentTemperatureAmerica(heatIndex);
    } else {
      const windchill = Math.round(calculateWindchill());
      setApparentTemperatureCanada(windchill);
      setApparentTemperatureAmerica(windchill);
    }
  }, [airTemperature, calculateHumidex, calculateHeatIndex, calculateWindchill]);

  function calculateApparentTemperature(event) {
    switch (event.target.name) {
      case "airTemperature":
        setAirTemperature(parseFloat(event.target.value));
        break;
      case "dewpoint":
        setDewpoint(parseFloat(event.target.value));
        setRelativeHumidity(Math.round(calculateRelativeHumidity()));
        break;
      case "relativeHumidity":
        setRelativeHumidity(parseFloat(event.target.value));
        setDewpoint(Math.round(calculateDewpoint()));
        break;
      case "windspeed":
        setWindspeed(parseFloat(event.target.value));
        break;
      default:
        return;
    }
  }

  /* See https://en.wikipedia.org/wiki/Humidex#Computation_formula
   * Takes air temperature and dewpoint in degrees Celsius.
   * Returns humidex (unitless, but references Celsius scale).
   */
  function calculateHumidex() {
    let humidex = airTemperature;

    // We don't need to concern ourselves with dewpoints below 10 or
    // air temperatures below 15°C.
    if (dewpoint < 10 || airTemperature < 15) {
      return humidex;
    }

    const exp = 2.71828;
    const expMultiplier = 5417.753;

    return (
      humidex +
      (5 / 9) *
        (6.11 *
          Math.pow(
            exp,
            expMultiplier * (1 / 273.16 - 1 / (273.15 + dewpoint))
          ) -
          10)
    );
  }

  /* See https://en.wikipedia.org/wiki/Heat_index#Formula
   * Takes air temperature in degrees Celsius and relative
   * humidity in percent between 0 and 100. Returns the
   * heat index in degrees Celsius.
   */
  function calculateHeatIndex() {
    // For air temperature below 27°C or relative humidity
    // below 40 we don't need to calculate heat index.
    if (airTemperature < 27 || relativeHumidity < 40) {
      return airTemperature;
    }

    // Using short variable names here will make it easier to read the equation.
    const T = airTemperature;
    const R = relativeHumidity;

    const c1 = -8.78469475556;
    const c2 = 1.61139411;
    const c3 = 2.33854883889;
    const c4 = -0.14611605;
    const c5 = -0.012308094;
    const c6 = -0.0164248277778;
    const c7 = 0.002211732;
    const c8 = 0.00072546;
    const c9 = -0.000003582;

    return (
      c1 +
      c2 * T +
      c3 * R +
      c4 * T * R +
      c5 * Math.pow(T, 2) +
      c6 * Math.pow(R, 2) +
      c7 * Math.pow(T, 2) * R +
      c8 * T * Math.pow(R, 2) +
      c9 * Math.pow(T, 2) * Math.pow(R, 2)
    );
  }

  /* See https://en.wikipedia.org/wiki/Wind_chill#North_American_and_United_Kingdom_wind_chill_index
   * Takes air temperature in degrees Celsius and windspeed
   * in km/h. Returns the windspeed relative to Celsius.
   */
  function calculateWindchill() {
    // Windchill is only defined below 10°C and windspeeds above 4.8km/h.
    if (airTemperature > 10 || windspeed < 4.8) {
      return airTemperature;
    }

    return (
      13.12 +
      0.6215 * airTemperature -
      11.37 * Math.pow(windspeed, 0.16) +
      0.3965 * airTemperature * Math.pow(windspeed, 0.16)
    );
  }

  /*
  function convertToDegreesFahrenheit(degreesCelsius) {
    return (degreesCelsius + 40) * 1.8 - 40;
  }

  function convertToDegreesCelsius(degreesFahrenheit) {
    return (degreesFahrenheit + 40) / 1.8 - 40;
  }
  */

  // See https://www.ajdesigner.com/phphumidity/dewpoint_equation_dewpoint_temperature.php
  function calculateDewpoint() {
    return (
      Math.pow(relativeHumidity / 100, 1 / 8) * (112 + 0.9 * airTemperature) +
      0.1 * airTemperature -
      112
    );
  }

  // See https://www.ajdesigner.com/phphumidity/dewpoint_equation_relative_humidity.php
  function calculateRelativeHumidity() {
    return (
      100 *
      Math.pow(
        (112 - 0.1 * airTemperature + dewpoint) / (112 + 0.9 * airTemperature),
        8
      )
    );
  }

  return (
    <main className={mainStyles.main}>
      
      <div className={mainStyles.feelslike}>
        <Apparent country="canada" temperature={apparentTemperatureCanada} units="°C" />
        <Apparent country="us" temperature={apparentTemperatureAmerica} units="°C" />
      </div>
      
      <form className={mainStyles.form}>
        <Slider
          min="-40"
          max="50"
          name="airTemperature"
          value={airTemperature}
          changeHandler={calculateApparentTemperature}
          className="actual"
          label="Actual Temperature"
          units="°C"
        />
        <Slider
          min="0"
          max="50"
          name="dewpoint"
          value={dewpoint}
          changeHandler={calculateApparentTemperature}
          label="Dewpoint"
          units="°C"
        />
        <Slider
          min="0"
          max="100"
          name="relativeHumidity"
          value={relativeHumidity}
          changeHandler={calculateApparentTemperature}
          label="Relative Humidity"
          units="%"
        />
        <Slider
          min="0"
          max="150"
          name="windspeed"
          value={windspeed}
          changeHandler={calculateApparentTemperature}
          label="Windspeed"
          units="km/h"
        />
      </form>
      
    </main>
  );
};

export default Main;
