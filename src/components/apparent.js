import React from "react";
import apparentStyles from "./apparent.module.css";

// props: country, temperature, units
const Apparent = (props) => {
  let country, emoji;
  
  switch (props.country.toLowerCase()) {
    case "canada":
      country = "Canada"
      emoji = "🇨🇦"
      break;
    default:
      country = "the U.S."
      emoji = "🇺🇸"
      break;
  }
  
  return(
    <div className={apparentStyles.apparent}>
      <p>Feels like</p>
      <h2>{props.temperature}{props.units}</h2>
      <p>for {country} <span role="img" aria-label={`Flag of ${country} emoji`}>{emoji}</span></p>
    </div>
  )
}

export default Apparent;