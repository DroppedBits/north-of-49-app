import React from "react";
import sliderStyles from "./slider.module.css";

// props: min, max, name, value, changeHandler, label, units
const Slider = (props) => {
  return (
    <div className={sliderStyles.input}>
      <div className={sliderStyles.slider}>
      <input
        type="range"
        min={props.min}
        max={props.max}
        id={props.name}
        name={props.name}
        value={props.value}
        onChange={e => props.changeHandler(e)}
      ></input>
      
      <label htmlFor={props.name}>
        {props.label}
      </label>
      </div>
      
      <strong className={sliderStyles.actual}>
        {props.value}
        {props.units}
      </strong>
    </div>
  );
}

export default Slider;