import React from "react";

export default () => (
  <footer>
    <p>Made with <span role="img" aria-label="love">❤️</span> in Canada, on <a href="https://glitch.com" >Glitch</a>.</p>
  </footer>
);
